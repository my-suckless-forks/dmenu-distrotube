static const char *colors[SchemeLast][2] = {
	                       /*     fg         bg       */
	[SchemeNorm]           = { "#EEFFFF", "#292D3E" },
	[SchemeSel]            = { "#242837", "#c792ea" },
	[SchemeSelHighlight]   = { "#44b9b1", "#1c1f2b" },
	[SchemeNormHighlight]  = { "#44b9b1", "#1c1f2b" },
	[SchemeOut]            = { "#1c1f2b", "#7986E7" },
	[SchemeMid]            = { "#d7d7d7", "#242837" },
};
